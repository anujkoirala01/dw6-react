import FirstComponent from "./FirstComponent";
import MyTraining from "./MyTraining";
import logo from "./logo.svg";
import MyName from "./MyName";
import LearnInLineCss from "./component/LearnInLineCss";
import LearnOr from "./component/LearnOr";
import SecondComponent from "./component/SecondComponent";

import StoreTag from "./component/StoreTag";
import MapPractice from "./component/MapPractice";
import LearnTernaryOperator from "./component/LearnTernaryOperator";
import LearnToHandleData from "./component/LearnToHandleData";
import LearnUseState1 from "./component/learnUseState/LearnUseState1";
import LearnUseState2 from "./component/learnUseState/LearnUseState2";
import LearnUseState3 from "./component/learnUseState/LearnUseState3";
import LearnUseState4 from "./component/learnUseState/LearnUseState4";
import LearnUseState5 from "./component/learnUseState/LearnUseState5";
import LearnUseState6 from "./component/learnUseState/LearnUseState6";
import LearnUseState7 from "./component/learnUseState/LearnUseState7";
import LearnUseState8 from "./component/learnUseState/LearnUseState8";
import LearnUseState9 from "./component/learnUseState/LearnUseState9";
import LearnUseState10 from "./component/learnUseState/LearnUseState10";
import LearnUseEffect1 from "./component/useEffect/LearnUseEffect1";
import LearnUseState11 from "./component/learnUseState/LearnUseState11";
import LearnUseEffect2 from "./component/useEffect/LearnUseEffect2";
import LearnUseEffect3 from "./component/useEffect/LearnUseEffect3";
import LearnUseEffect4 from "./component/useEffect/LearnUseEffect4";
import LearnUseEffect5 from "./component/useEffect/LearnUseEffect5";
import LearnUseEffect6 from "./component/useEffect/LearnUseEffect6";
import LearnUseEffect7 from "./component/useEffect/LearnUseEffect7";
import CleanUpFunction from "./component/useEffect/CleanUpFunction";
import { useState } from "react";
import CleanUpFunction2 from "./component/useEffect/CleanUpFunction2";
import LearnInfiniteLoop from "./component/learnUseState/LearnInfiniteLoop";
import LearnForm1 from "./component/learnForm/LearnForm1";
import LearnForm2 from "./component/learnForm/LearnForm2";
import LearnForm3 from "./component/learnForm/LearnForm3";
import LearnForm4 from "./component/learnForm/LearnForm4";
import LearnForm5 from "./component/learnForm/LearnForm5";
import LearnForm6 from "./component/learnForm/LearnForm6";
import ReadContacts from "./component/learnForm/ReadContacts";
import Routing1 from "./component/learnRouting/Routing1";
import LearnForm7 from "./component/learnForm/LearnForm7";
import FormikForm from "./component/learnFormik/formikForm";
import FormikTutorial from "./component/learnFormik/FormikTutorial";
import LearnRoute from "./component/learnRouting/routingPractice/LearnRoute";
import NestingRoutes from "./component/learnRouting/routingPractice/NestingRoutes";
import LearnUseRef1 from "./component/LearnUseRef/LearnUseRef1";
import Parent from "./propDrilling/Parent";
import HideAndShowPassword from "./component/learnForm/HideAndShowPassword";
import HideAndShow2 from "./component/learnForm/HideAndShow2";

// alt + shift + O : removes unused imports

// import LearnOnclick from "./component/LearnOnclick";

function App() {
  let [show, setShow] = useState(true);
  return (
    <div>
      <h1 className="text-blue-700">This is React APP</h1>
      {/* <a href="https://www.youtube.com/" target="_blank">
        youtube
      </a>
      <FirstComponent></FirstComponent>
      <MyTraining></MyTraining> */}
      {/* <SecondComponent name="Anuj" country="Nepal" age={24}></SecondComponent> */}
      {/* <LearnOr></LearnOr> */}
      {/* <LearnInLineCss></LearnInLineCss> */}
      {/* <LearnOnclick></LearnOnclick> */}
      {/* <StoreTag></StoreTag> */}
      {/* <MapPractice></MapPractice> */}
      {/* <LearnTernaryOperator></LearnTernaryOperator> */}
      {/* <LearnToHandleData></LearnToHandleData> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <LearnUseState2></LearnUseState2> */}
      {/* <LearnUseState3></LearnUseState3> */}
      {/* <LearnUseState4></LearnUseState4> */}
      {/* <LearnUseState6></LearnUseState6> */}
      {/* <LearnUseState7></LearnUseState7> */}
      {/* <LearnUseState8></LearnUseState8> */}
      {/* <LearnUseState9></LearnUseState9> */}
      {/* <LearnUseState10></LearnUseState10> */}
      {/* <LearnUseState11></LearnUseState11> */}
      {/* <LearnUseEffect1></LearnUseEffect1> */}
      {/* <LearnUseEffect2></LearnUseEffect2> */}
      {/* <LearnUseEffect3></LearnUseEffect3> */}
      {/* <LearnUseEffect4></LearnUseEffect4> */}
      {/* <LearnUseEffect5></LearnUseEffect5> */}
      {/* <LearnUseEffect6></LearnUseEffect6> */}
      {/* <LearnUseEffect7></LearnUseEffect7> */}

      {/* {show?<CleanUpFunction></CleanUpFunction>:null}

      <button
      onClick={()=>{
        setShow(true)
      }}>Show</button>
      
      <button
      onClick={()=>{
        setShow(false)
      }}>Hide</button> */}

      {/* {show?<CleanUpFunction2></CleanUpFunction2>:null}

      <button
      onClick={()=>{
        setShow(true)
      }}>Show</button>
      
      <button
      onClick={()=>{
        setShow(false)
      }}>Hide</button> */}

      {/* <LearnInfiniteLoop></LearnInfiniteLoop> */}
      {/* <LearnForm1></LearnForm1> */}
      {/* <LearnForm2></LearnForm2> */}
      {/* <LearnForm3></LearnForm3> */}
      {/* <LearnForm4></LearnForm4> */}
      {/* <LearnForm5></LearnForm5> */}
      <LearnForm6></LearnForm6>
      {/* <ReadContacts></ReadContacts> */}
      {/* <Routing1></Routing1> */}
      {/* <LearnForm7></LearnForm7> */}
      {/* <FormikForm></FormikForm> */}
      {/* <FormikTutorial></FormikTutorial> */}
      {/* <LearnRoute></LearnRoute> */}
      {/* <NestingRoutes></NestingRoutes> */}
      {/* <Parent></Parent> */}
      {/* <HideAndShowPassword></HideAndShowPassword> */}
      {/* <HideAndShow2></HideAndShow2> */}
    </div>
  );
}

export default App;
