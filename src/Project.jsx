import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Outlet, Route, Routes } from "react-router-dom";
import { changeCity, changeProvince } from "./features/addressSlice";
import { changeBlogTitle } from "./features/blogSlice";
import { changeAge, changeName } from "./features/counterSlice";
import { changeProductName, changeProductPrice } from "./features/productSlice";
import Footer from "./projectComponent/Footer";
import NavBar from "./projectComponent/NavBar";
import UpdateProduct from "./projectComponent/UpdateProduct";
import ShowAllProductUsingRTK from "./projectRTK/ShowAllProductUsingRTK";
import ViewProductRTK from "./projectRTK/ViewProductRTK";
import CreateProductRTK from "./projectRTK/CreateProductRTK";

const Project = () => {
  let dispatch = useDispatch();

  let info = useSelector((store) => {
    return store.info;
  });

  let addressData = useSelector((store) => {
    return store.address;
  });

  let productData = useSelector((store) => {
    return store.product;
  });

  let blogData = useSelector((store) => {
    return store.blog;
  });
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <NavBar></NavBar>
              <Outlet></Outlet> <Footer></Footer>
            </div>
          }
        >
          <Route index element={<div>Home Page</div>}></Route>
          <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            {/* <Route index element={<div><ReadProduct></ReadProduct></div>}></Route> */}
            <Route
              index
              element={<ShowAllProductUsingRTK></ShowAllProductUsingRTK>}
            ></Route>
            <Route
              path=":id"
              element={<ViewProductRTK></ViewProductRTK>}
            ></Route>
            <Route
              path="create"
              element={<CreateProductRTK></CreateProductRTK>}
            ></Route>
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route index element={<div>Update Product</div>}></Route>
              <Route
                path=":id"
                element={<UpdateProduct></UpdateProduct>}
              ></Route>
            </Route>
          </Route>
        </Route>
      </Routes>
      <div className="bg-orange-200">
        <h1 className="text-3xl font-bold">Tailwind Check</h1>
        Name:{info.name}
        <br />
        Age:{info.age}
        <br />
        City:{addressData.city}
        <br />
        Province:{addressData.province}
        <br />
        Product Name:{productData.name}
        <br />
        Product Price:{productData.price}
        <br />
        Blog Title:{blogData.title}
        <br />
        <button
          onClick={() => {
            dispatch(changeName("Anuj Koirala"));
          }}
          className="custom-btn"
          >
          Change Name
        </button>
        <br />
        <button
          onClick={() => {
            dispatch(changeAge(25));
          }}
          className="custom-btn"
          >
          Change Age
        </button>
        <br />
        <button
          onClick={() => {
            dispatch(changeCity("Lalitpur"));
          }}
          className="custom-btn"
          >
          Change City
        </button>
        <br />
        <button
          onClick={() => {
            dispatch(changeProvince(3));
          }}
        className="custom-btn"
        >
          Change Province
        </button>
        <br />
        <button
          onClick={() => {
            dispatch(changeProductName("Samsung"));
          }}
          className="custom-btn"
          >
          Change Product Name
        </button>
        <br />
        <button
          onClick={() => {
            dispatch(changeProductPrice(1000));
          }}
          className="custom-btn"
          >
          Change Product Price
        </button>
        <br />
        <button
          onClick={() => {
            dispatch(changeBlogTitle("Blog 1"));
          }}
          className="custom-btn"
          >
          Change Blog Title
        </button>
      </div>
    </div>
  );
};

export default Project;
