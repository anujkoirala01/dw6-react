import { createSlice } from "@reduxjs/toolkit"

const initialStateValue ={
    city:"Kathmandu",
    province:3
}

export const counterSlice = createSlice({
    // give unique name
    name:"address",
    initialState:initialStateValue,
    reducers:{
        changeCity : (state,action)=>{
            state.city = action.payload

        },
        changeProvince:(state,action)=>{
            state.province = action.payload
        } 
    }

})

export const {changeCity,changeProvince} = counterSlice.actions

export default counterSlice.reducer