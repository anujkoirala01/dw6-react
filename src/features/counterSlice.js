import { createSlice } from "@reduxjs/toolkit"

// The main task of Slice is to initialize 
// variable and to re-initialize variable
const initialStateValue ={
    name:"Anuj",
    age:24,
    isMarried:false
}

export const counterSlice = createSlice({
    // give unique name
    name:"infoSlice",
    initialState:initialStateValue,
    reducers:{
        changeName:(state, action)=>{
            state.name = action.payload
        },
        changeAge:(state,action)=>{
            state.age = action.payload
        }
    }

})

export const {changeName, changeAge} = counterSlice.actions

export default counterSlice.reducer