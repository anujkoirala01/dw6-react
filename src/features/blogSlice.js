import { createSlice } from "@reduxjs/toolkit"

// The main task of Slice is to initialize 
// variable and to re-initialize variable
const initialStateValue ={
   title:"",
   description:""
}

export const blogSlice = createSlice({
    // give unique name
    name:"blog",
    initialState:initialStateValue,
    reducers:{
        changeBlogTitle:(state, action)=>{
            state.title = action.payload
        },
        changeBlogDescription:(state,action)=>{
            state.description = action.payload
        }
    }

})

export const {changeBlogTitle, changeBlogDescription} = blogSlice.actions

export default blogSlice.reducer