import { createSlice } from "@reduxjs/toolkit"

// The main task of Slice is to initialize 
// variable and to re-initialize variable
const initialStateValue ={
    name:"Apple",
    price:10000,
    quantity:1
}

export const productSlice = createSlice({
    // give unique name
    name:"product",
    initialState:initialStateValue,
    reducers:{
        changeProductName:(state,action)=>{
            state.name= action.payload

        },
        changeProductPrice:(state,action)=>{
            state.price= action.payload

        },
        changeProductQuantity:(state,action)=>{
            state.quantity= action.payload

        }
    }
       

})

export const {changeProductName,changeProductPrice,changeProductQuantity} = productSlice.actions

export default productSlice.reducer