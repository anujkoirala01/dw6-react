import React from 'react'

const LearnOnclick = () => {
  return (
    <div>
      <button onClick={()=>{
        console.log("My name is Anuj Koirala.")
      }}>
        Click Me
      </button>
    </div>
  )
}

export default LearnOnclick