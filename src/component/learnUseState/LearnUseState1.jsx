import React, { useState } from 'react'

const LearnUseState1 = () => {

    // let name = "mern"

    // define variable using useState

    let [name, setName] = useState("mern")
    let [surname, _setName] = useState("stack")
  return (
    <div>
        {name} 
        <button
        onClick={()=>{
            setName("MERN")
        }}>Change Name</button>
        <br></br>
        {surname}
        <button
        onClick={()=>{
            _setName("STACK")
        }}>
            Change Surname
        </button>
    </div>
  )
}

export default LearnUseState1