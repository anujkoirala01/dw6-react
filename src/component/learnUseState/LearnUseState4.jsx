import React, { useState } from 'react'

const LearnUseState4 = () => {

    let [show, setShow] = useState(true)
  return (
    <div>
        {
            show?<img src="./favicon.ico" alt="favicon" />:null
        }
        <br />
        <button
        onClick={()=>{
            setShow(!show)
        }}>{show?"hide":"show"}</button>
    </div>
  )
}

export default LearnUseState4