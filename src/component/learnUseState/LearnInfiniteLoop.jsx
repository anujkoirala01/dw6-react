import React, { useEffect, useState } from "react";

// always put setCount on event or useEffect, otherwise infinite loop occurs
const LearnInfiniteLoop = () => {
  let [count, setCount] = useState(0);

  //   setCount(count+1)      ; generates error as too many re-renders

  useEffect(()=>{
    setCount(count+1)

  },[])

  return (
    <div>
      count = {count}
      <br />
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnInfiniteLoop;
