import React, { useState } from 'react'

const LearnUseState10 = () => {
    let [count, setCount] = useState([1,2])
    console.log("Hello")

    
  return (
    <div>

        count = {count}
        <br />
        <button
        onClick={()=>{
            setCount([1,2])
        }}>Increment count</button>


    </div>
  )
}

export default LearnUseState10