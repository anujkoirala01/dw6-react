import React, { useState } from 'react'
import LearnUseState5 from './LearnUseState5'

const LearnUseState6 = () => {

    let [show, setShow] = useState(true)


  return (
    <div>

        {
            show?<LearnUseState5></LearnUseState5>:null
        }

        <button
        onClick={()=>{
            setShow(!show)

        }}>{show?"hide":"show"}</button>


    </div>
  )
}

export default LearnUseState6