import React from 'react'
import { useState } from 'react';

const LearnUseState8 = () => {

    let [count1, setCount1] = useState(0);

    console.log("Hello")
  return (
    <div>
    count1 is {count1}
    <br></br>
    <button
    onClick={()=>{
        setCount1(count1+1)
    }}>Increment count1</button>

    <button onClick={()=>{
        setCount1(0)
    }}>Reset</button>
</div>
  )
}
// A page will be rendered, if state variable is changed
export default LearnUseState8