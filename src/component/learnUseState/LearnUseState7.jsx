import React, { useState } from 'react'

const LearnUseState7 = () => {

    let [count1, setCount1] = useState(0);
  return (
    <div>
        count1 is {count1}
        <br></br>
        <button
        onClick={()=>{
            setCount1(count1+1)
        }}>Increment count1</button>
    </div>
  )
}

// when setXXX is called, page is rendered

export default LearnUseState7