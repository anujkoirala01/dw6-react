import React, { useState } from 'react'

const LearnUseState9 = () => {
    let [count1, setCount1] = useState(0)
    let [count2, setCount2] = useState(100)



  return (
    <div>
        count1 = {count1}
        <button 
        onClick={()=>{
            setCount1(count1+1)
        }}>Increment Count1</button>
        <br></br>

        count2 = {count2}
        <button
        onClick={()=>{
            setCount2(count2+1)
        }}>Increment Count2</button>
       
    </div>
  )
}
 // When a set is called, its corresponding state variable
 // is updated and the other state variables store the 
 // previous value when rendered


export default LearnUseState9