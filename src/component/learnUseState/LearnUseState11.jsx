import React, { useState } from 'react'

const LearnUseState11 = () => {
    let [count,setCount] = useState(0)
    console.log("Hello")


  return (
    <div>
        count = {count}
        <br />
        <button
        onClick={()=>{
            // setCount(1)
            // setCount(2)
            // setCount(3)
            // Another way
            // setCount(()=>{
            //     return count+1
            // })

            // setCount(count+1) // 0+1 = 1
            // setCount(count+1) // 0+1 = 1
            // setCount(count+1) // 0+1 = 1

            setCount((pre)=>{
                return pre+1
            })
            setCount((pre)=>{
                return pre+1
            })
            setCount((pre)=>{
                return pre+1
            })

            // pre --> always indicate extra memory

        }}>Increment Count</button>


    </div>
  )
}

// component memory
// 0

// extra memory
// 0,1,2,3 --> 3


// extra memory --- until the LOC finishes, every change in 
// variable is stored in extra memory

export default LearnUseState11