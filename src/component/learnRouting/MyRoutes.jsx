import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import GetDynamicRoutes from "./GetDynamicRoutes";
import ReadContacts from "../learnForm/ReadContacts";
import ContactDetails from "./ContactDetails";
import CreateContacts from "./CreateContacts";
import UpdateContacts from "./UpdateContacts";
import LearnForm6 from "../learnForm/LearnForm6";

const MyRoutes = () => {
  return (
    <div>
      {" "}
      {/* to have different component for different link */}
      <Routes>
        <Route path="/" element={<Home></Home>}></Route>
        <Route path="/about" element={<About></About>}></Route>
        <Route path="/contact" element={<Contact></Contact>}></Route>
        <Route path="/contact" element={<div>Hello</div>}></Route>
        {/* the upper /contact is executed rather the lower one, gives priority to the first one  */}

        <Route
          path="/contact/:id1/ram/:id2"
          element={<GetDynamicRoutes></GetDynamicRoutes>}
        ></Route>
        {/* dynamic routing, ':' */}

        {/* If conflict occurs while choosing component, more specific route component is taken */}
        {/* The route below this comment is selected rather the one above this comment */}

        <Route
          path="/contact/a"
          element={<div>This is /contact/a</div>}
        ></Route>

        {/* <Route path="*" element={<div>Error 404</div>}></Route>
    <Route path='/blogs' element={<div>Blogs</div>}></Route>
    <Route path='/blogs/:id' element={<div>Blogs/:id</div>}></Route>
    <Route path='/blogs/create' element={<div>Blogs/create</div>}></Route>
    <Route path='/blogs/update/:id' element={<div>Blogs/update/:id</div>}></Route> */}

        <Route
          path="blogs"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route index element={<div>Read all Blog</div>}></Route>
          <Route path=":id" element={<div>Detail page</div>}></Route>
          <Route path="create" element={<div>Create blog</div>}></Route>
          <Route
            path="update"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route path=":id" element={<div>Update page</div>}></Route>
          </Route>
        </Route>

        <Route
          path="products"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route index element={<div>Read all products</div>}></Route>
          <Route path=":id" element={<div>Detail Page</div>}></Route>
          <Route path="create" element={<div>Create Page</div>}></Route>
          <Route
            path="update"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route path=":id" element={<div>Products update</div>}></Route>
          </Route>
        </Route>

        <Route
          path="contacts"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route index element={<div><ReadContacts></ReadContacts></div>}></Route>
          <Route path=":id" element={<div><ContactDetails></ContactDetails></div>}></Route>
          <Route path="create" element={<div><LearnForm6></LearnForm6></div>}></Route>
          <Route
            path="update"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route path=":id" element={<div><UpdateContacts></UpdateContacts></div>}></Route>
          </Route>
        </Route>

        
      </Routes>
    </div>
  );
};

export default MyRoutes;
