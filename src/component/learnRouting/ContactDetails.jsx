import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const ContactDetails = () => {
  let params = useParams(); // get parameter of url
  let [details, setDetails] = useState({}) // only one detail, so is object --> {} inside useState

  let id = params.id;

  let getDetails = async()=>{
    let response = await axios({
      url: `http://localhost:8000/api/v1/contacts/${id}`,
      method : "GET"

    })
    setDetails(response.data.data)
  }

  useEffect(()=>{

    getDetails()

  },[])

  // async cannot be used in useEffect, so we have to create a new function

  return <div>
    Detail
    <p>Address: {details.address}</p>
    <p>Email: {details.email}</p>
    <p>phoneNumber: {details.phoneNumber}</p>

  </div>;
};

export default ContactDetails;
