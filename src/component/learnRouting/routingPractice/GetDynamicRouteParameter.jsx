import React from 'react'
import { useParams } from 'react-router-dom'


/* useParams() --> to get dynamic route parameters
useSearchParams() --> to get query parameters
useNavigate() --> to change patch onClick event
)
 */
const GetDynamicRouteParameter = () => {
    const params = useParams() // params is a object
    console.log(params)
  return (
    <div>
        Get Dynamic Route Parameters
        <br />
        {params.id1}
        <br />
        {params.id2}
    </div>
  )
}

export default GetDynamicRouteParameter