import React from 'react'
import { Outlet, Route, Routes } from 'react-router-dom'
import Home from './Home'
import Error from './Error'

const NestingRoutes = () => {
  return (
    <div>
        <Routes>
            <Route path='/'element={<div><Outlet></Outlet></div>}>
                <Route path='student' element={<div><Outlet></Outlet></div>}>
                    <Route index element={<div>This is student page</div>}></Route>
                    <Route path='1' element={<div>This is student 1 page</div>}></Route>
                    <Route path='nitan' element={<div>This is nitan page</div>}></Route>
                </Route>
                <Route index element={<Home></Home>}></Route>
                <Route path="*" element={<Error></Error>}></Route>
            </Route>
        </Routes>
    </div>
  )
}

export default NestingRoutes