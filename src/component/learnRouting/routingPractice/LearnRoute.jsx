import React from 'react'
import { NavLink, Route, Routes } from 'react-router-dom'
import Home from './Home'
import About from './About'
import Contact from './Contact'
import Error from './Error'
import GetDynamicRouteParameter from './GetDynamicRouteParameter'

const LearnRoute = () => {
  return (
    <div>
        <NavLink to="/" style={{marginLeft:"20px"}}>Home</NavLink>
        <NavLink to="/about" style={{marginLeft:"20px"}}>About</NavLink>
        <NavLink to="/contact" style={{marginLeft:"20px"}}>Contact</NavLink>



        <Routes>
            <Route path='/' element={<Home></Home>}></Route>
            <Route path='/about' element={<About></About>}></Route>
            <Route path='/contact' element={<Contact></Contact>}></Route>
            <Route path='*' element={<Error></Error>}></Route>
            <Route path='/b/:id1/id/:id2' element={<GetDynamicRouteParameter></GetDynamicRouteParameter>}></Route>
        </Routes>
    </div>
  )
}

export default LearnRoute