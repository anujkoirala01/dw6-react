import React from 'react'
import { NavLink } from 'react-router-dom'

const MyNavLink = () => {
  return (
    <div>{/* change url on click of link */}
    <NavLink to="/" style={{ marginRight: "100px" }}>
      Home
    </NavLink>
    <NavLink to="/about" style={{ marginRight: "100px" }}>
      About Us
    </NavLink>
    <NavLink to="/contacts" style={{ marginRight: "100px" }}>
      Read all contacts
    </NavLink>
    <NavLink to="/contacts/create" style={{ marginRight: "100px" }}>
      Create Contacts
    </NavLink>
    
    
    </div>
  )
}

export default MyNavLink