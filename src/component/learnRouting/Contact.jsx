import React from "react";
import { useSearchParams } from "react-router-dom";

const Contact = () => {


// Get Query Parameter
  let[queryParameter] = useSearchParams()

  console.log(queryParameter.get('name'))
  console.log(queryParameter.get('age'))
  console.log(queryParameter.get('address'))
  return <div>Contact</div>;
};

export default Contact;
