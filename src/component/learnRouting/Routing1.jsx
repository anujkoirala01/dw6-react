import React from "react";
import MyNavLink from "./MyNavLink";
import MyRoutes from "./MyRoutes";
// alt + shift + O : removes unused imports
const Routing1 = () => {
  return (
    <div>
      {/* NavLink Component */}
      <MyNavLink></MyNavLink>

      {/* Routes Component */}

      <MyRoutes></MyRoutes>
    </div>
  );
};

export default Routing1;
