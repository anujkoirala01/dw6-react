import React from "react"; // rafce --> shortcut

// To perform JS operation in any tag, we must use {}

const SecondComponent = (props) => {
  //   console.log("Hello");
  console.log(props);
  return (
    <div>
      <p>Name is {props.name} </p>
      <p>Country is {props.country} </p>
      <p>Age is {props.age} </p>
    </div>
  );
};

// JS function cannot return two values i.e it returns only one value

export default SecondComponent;
