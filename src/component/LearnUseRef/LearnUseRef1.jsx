import React, { useRef } from "react";

const LearnUseRef1 = () => {
  let inputRef1 = useRef();
  let inputRef2 = useRef();
  let nameRef = useRef();
  /* Defining Ref */
  return (
    <div>
      <form>
        <div ref={nameRef}>My name is Anuj Koirala</div>

        <input ref={inputRef1} type="text"></input>
        <br />
        <input ref={inputRef2} type="text"></input>
        {/* Attaching ref */}
        <br />

        <button
          type="button"
          onClick={() => {
            inputRef1.current.focus();
          }}
        >
          Click Me to focus input 1
        </button>
        <br />

        <button
          type="button"
          onClick={() => {
            inputRef2.current.focus();
            inputRef2.current.style.backgroundColor = "red";
          }}
        >
          Click Me to focus input 2
        </button>
        <br />
        <button
          type="button"
          onClick={() => {
            nameRef.current.style.color = "red";
          }}
        >
          Change color of div
        </button>
      </form>
    </div>
  );
};

export default LearnUseRef1;
