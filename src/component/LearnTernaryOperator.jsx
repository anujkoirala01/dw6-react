import React from 'react'

const LearnTernaryOperator = () => {
    let isMarried = false

    let age = 18

    let num = 85


  return (
    <div>

        {
            // isMarried?<div>He is married.</div> : <div>He is not married.</div>
        }

        {
            // age===16?<div>His age is 16.</div>:age===17?<div>His age is 17.</div>
            // :age===18?<div>His age is 18.</div>:<div>Out of list</div>
        }

        {
            num<=39?<div>Fail</div>:num>=40 && num<=59?<div>Third Division</div>
            :num>=60 && num<=79?<div>First Division</div>:<div>Distinction</div>
        }




    </div>
  )
}

export default LearnTernaryOperator