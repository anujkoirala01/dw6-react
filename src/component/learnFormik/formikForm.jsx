import React from "react";
import { Field, Form, Formik } from "formik";
import * as yup from "yup";
import FormikInput from "./FormikInput";
import FormikTextArea from "./FormikTextArea";
import FormikSelect from "./FormikSelect";
import FormikRadio from "./FormikRadio";
import FormikCheckbox from "./FormikCheckbox";

const FormikForm = () => {
  let initialValues = {
    firstName: "",
    lastName: "",
    description: "",
    country:"",
    gender:"",
    isMarried:false
  };

  let onSubmit = (value, other) => {
    console.log(value);
  };

  let validationSchema = yup.object({
    firstName: yup
      .string()
      .required("First Name is required"),
    // firstName: yup.string("First Name must be a number").required("First Name is required"),
    lastName: yup.string().required("Last Name is required"),
    // name: yup.string().required("Name field is required"),
    // address: yup.string().required("Address field is required"),
    description: yup.string().required("Description is required"),
    // age: yup.number().required("Age field is required")
  });

  let countryOption = [
    {
      label: "Select Country",
      value: "",
      disabled: true,
    },
    {
      label: "Nepal",
      value: "Nep",
    },
    {
      label: "India",
      value: "Ind",
    },
    {
      label: "China",
      value: "Chi",
    },
    {
      label: "Japan",
      value: "Jap",
    },
    {
      label: "USA",
      value: "US",
    },
    {
      label: "Germany",
      value: "Ger",
    },
    {
      label: "Korea",
      value: "Kor",
    },
  ]

  let genderOption = [
    {
      label: "Male",
      value: "male",
    },
    {
      label: "Female",
      value: "female",
    },
    {
      label: "Others",
      value: "others",
    },
  ]

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="firstName"
                label="First Name"
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("firstName", e.target.value);
                }}
                placeholder="firstName"
                style={{ backgroundColor: "lightblue" }}
                required={true}
              ></FormikInput>

              <FormikInput
                name="lastName"
                label="Last Name"
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("lastName", e.target.value);
                }}
                required={true}
              ></FormikInput>

              <FormikTextArea
                name="description"
                label="Description"
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("description", e.target.value);
                }}
                required={true}
              ></FormikTextArea>
              

              <FormikSelect
                name="country"
                label="Country"
                onChange={(e) => {
                  formik.setFieldValue("country", e.target.value);
                }}

                required={true}
                options={countryOption}
              ></FormikSelect>

              
              <FormikRadio
                name="gender"
                label="Gender"
                onChange={(e) => {
                  formik.setFieldValue("gender", e.target.value);
                }}

                required={true}
                options={genderOption}
              ></FormikRadio>

              <FormikCheckbox
                name="isMarried"
                label="Is Married?"
                onChange={(e) => {
                  formik.setFieldValue("isMarried", e.target.checked);
                }}
                required={true}
              ></FormikCheckbox>
              


              {/* <Field name="firstName">
                {({ field, form, meta }) => {
                  return (
                    <div>
                      <label htmlFor="firstName">First Name</label>
                      <input
                      {...field}
                        id="firstName"
                        type="text"
                        placeholder="First Name"
                        value={meta.value}
                        onChange={field.onChange}
                        // onChange={(e) => {
                        //   formik.setFieldValue("firstName", e.target.value);
                        // }}
                      ></input>
                      {meta.touched && meta.error?(<div style={{color:"red"}}>First Name is required</div>):null}
                    </div>
                  );
                }}
              </Field> */}

              {/* <Field name="lastName">
                {({ field, form, meta }) => {
                  return (
                    <div>
                      <label htmlFor="lastName">Last Name</label>
                      <input
                      {...field}
                        id="lastName"
                        type="text"
                        placeholder="Last Name"
                        value={meta.value}
                        onChange={(e) => {
                          formik.setFieldValue("lastName", e.target.value);
                        }}
                      ></input>
                      {meta.touched && meta.error?(<div style={{color:"red"}}>Last Name is required</div>):null}
                    </div>
                  );
                }}
              </Field> */}

              {/* <Field name="description">
                {({ field, form, meta }) => {
                  return (
                    <div>
                      <label htmlFor="description">Description</label>
                      <input
                      {...field}
                        id="description"
                        type="text"
                        placeholder="Description"
                        value={meta.value}
                        onChange={(e) => {
                          formik.setFieldValue("description", e.target.value);
                        }}
                      ></input>

                      {meta.touched && meta.error  ?(<div style={{color:"red"}}>Description is required</div>):null}

                      
                    </div>
                    
                  );
                }}
              </Field> */}

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikForm;
