import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "./FormikInput";
import FormikTextArea from "./FormikTextArea";
import FormikSelect from "./FormikSelect";
import FormikRadio from "./FormikRadio";
import FormikCheckbox from "./FormikCheckbox";

const FormikTutorial = () => {
  let initialValues = {
    fullName: "",
    email: "",
    password: "",
    gender: "male",
    country: "nepal",
    isMarried: false,
    description: "",
    phoneNumber: "",
    age: 0,
  };

  let onSubmit = (value, other) => {
    console.log(value);
  };

  /* Regex --> it defines pattern in validation schema */
  let validationSchema = yup.object({
    fullName: yup
      .string()
      .required("Full Name is required")
      .min(10, "Must be at least 10 characters.")
      .max(15, "Must be at most 15 characters.")
      .matches(/^[a-zA-Z ]*$/, "Only alphabet and space is allowed"),
    email: yup
      .string()
      .required("Email is required")
      .matches(
        /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
        "Email is not valid"
      ),
    password: yup
      .string()
      .required("Password is required")
      .min(7, "Password length must be at least 7")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/,
        "Minimum eight and maximum 20 characters, at least one uppercase letter, one lowercase letter, one number and one special character"
      ),
    isMarried: yup.boolean(),
    country: yup.string().required("Country is required."),
    gender: yup.string().required("Gender is required"),
    description: yup.string(),
    phoneNumber: yup.string().required("Phone Number is required.")
    .matches(/^[0-9]+$/,"Only numbers allowed.") // The order for validation should be considered.
    .min(10,"Must be at least 10")
    .max(10,"Must be at most 10"),
    age: yup.number().required("Age is required").min(18, "Must be 18+"),
  });

  let genderOptions = [
    {
      label: "Male",
      value: "male",
    },
    {
      label: "Female",
      value: "female",
    },
    {
      label: "Other",
      value: "other",
    },
  ];

  let countryOptions = [
    {
      label: "Select Country",
      value: "",
      disabled: true,
    },
    {
      label: "Nepal",
      value: "nepal",
    },
    {
      label: "India",
      value: "india",
    },
    {
      label: "Japan",
      value: "japan",
    },
    {
      label: "Norway",
      value: "norway",
    },
  ];

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          // name
          // label
          // type
          // onchange
          // required
          // option(radio,select) --> array of object
          return (
            <Form>
              <FormikInput
                name="fullName"
                label="Full Name"
                type="text"
                // onChange={(e)=>{
                //     formik.setFieldValue("fullName",e.target.value)
                // }}
                required={true}
              ></FormikInput>

              <FormikInput
                name="email"
                label="Email"
                type="email"
                // onChange={(e)=>{
                //     formik.setFieldValue("email",e.target.value)
                // }}
                required={true}
              ></FormikInput>

              <FormikInput
                name="password"
                label="Password"
                type="password"
                // onChange={(e)=>{
                //     formik.setFieldValue("password",e.target.value)
                // }}
                required={true}
              ></FormikInput>

              <FormikRadio
                name="gender"
                label="Gender"
                //no type in radio, checkbox and select
                // onChange={(e)=>{
                //     formik.setFieldValue("gender",e.target.value)
                // }}
                required={true}
                options={genderOptions}
              ></FormikRadio>

              <FormikSelect
                name="country"
                label="Country"
                // onChange={(e)=>{
                //   formik.setFieldValue("country", e.target.value)
                // }}
                required={true}
                options={countryOptions}
              ></FormikSelect>

              <FormikCheckbox
                name="isMarried"
                label="Is Married?"
                // onChange={(e)=>{
                //   formik.setFieldValue("isMarried",e.target.checked)
                // }}
              ></FormikCheckbox>

              <FormikTextArea
                name="description"
                label="Description"
                // onChange={(e)=>{
                //   formik.setFieldValue("description",e.target.value)
                // }}
              ></FormikTextArea>

              <FormikInput
                name="age"
                label="Age"
                type="number"
                // onChange={(e)=>{
                //     formik.setFieldValue("age",e.target.value)
                // }}
                required={true}
              ></FormikInput>

              <FormikInput
                name="phoneNumber"
                label="Phone Number"
                type="text"
                // onChange={(e)=>{
                //     formik.setFieldValue("phoneNumber",e.target.value)
                // }}
                required={true}
              ></FormikInput>

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikTutorial;
