import React, { useEffect, useState } from "react";

const LearnUseEffect5 = () => {
  let [ar1, setAr1] = useState([1, 2]); //[1,2]
  console.log("i am component");

  //always use primitive dependency
  // if the dependency is non-primitive  make it primitive by using JSON.stringify() method

  useEffect(() => {
    console.log("i am useEffect");
  }, [JSON.stringify(ar1)]);
  // '[1,2]' => '[1,2]'
  return (
    <div>
      LearnUseEffect5
      <br></br>
      <button
        onClick={() => {
          setAr1([1, 2]);
        }}
      >
        Change ar1
      </button>
    </div>
  );
};

export default LearnUseEffect5;
