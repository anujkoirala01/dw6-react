import React, { useEffect } from "react";

//clean up function feature
//first clean up function execute then function execute
// but it does not execute in  first render
// when component gets unmount(hidden)
// nothing gets executed except clean up function

const CleanUpFunction = () => {

  console.log("I am component.")
  
  useEffect(()=>{
    console.log("I am useEffect.")

    return ()=>{
      console.log("I am clean up function.")
    }

  },[])
  return <div>CleanUpFunction</div>;
};

export default CleanUpFunction;
