import React, { useEffect } from 'react'


//useEffect is asynchronous function : the function that executes at last

const LearnUseEffect4 = () => {


    useEffect(()=>{
        console.log("i am use Effect")
    })

    console.log("i am component")


  return (
    <div>LearnUseEffect4</div>
  )
}

export default LearnUseEffect4