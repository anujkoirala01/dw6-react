import React, { useEffect, useState } from 'react'



//clean up function
//useEffect with dependency non-primitive
//useEffect is asynchronous
// Life cycle method


//1 . Syntax
//2 . Flow
const LearnUseEffect3 = () => {
    let [count, setCount] = useState(0)
    let [count1, setCount1] = useState(100)
    console.log("I am component")

    useEffect(()=>{
        console.log("I am useEffect")
    }, [count])

    // if one of the dependencies change, useEffect will run
    
  return (
    <div>
        count = {count}
        <br />
        count1 = {count1}
        <br />

        <button 
        onClick={()=>{
            setCount((pre)=>{
                return pre+1
            })
        }}>Increment Count</button>

        <br />
        <button
        onClick={()=>{
            setCount1((pre)=>{
                return pre+1
            })

        }}>Increment count1</button>


    </div>
  )
}

export default LearnUseEffect3