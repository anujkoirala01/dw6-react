import React, { useEffect, useState } from "react";



const LearnUseEffect7 = () => {
  let [count, setCount] = useState(0);
  useEffect(() => {
    console.log("i am useEffect");
    console.log("a");
    return () => {
      console.log("i am  clean up function i will execute first");
    };
  }, [count]);

  
  return (
    <div>
      count is {count}
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnUseEffect7;
