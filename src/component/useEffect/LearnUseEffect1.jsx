import React, { useEffect, useState } from 'react'


//1 . Syntax
//2 . Flow
const LearnUseEffect1 = () => {
    let [count, setCount] = useState(0)
    console.log("I am component")

    useEffect(()=>{
        console.log("I am useEffect")
    }, [])
  return (
    <div>
        count = {count}
        <br />

        <button 
        onClick={()=>{
            setCount((pre)=>{
                return pre+1
            })
        }}>Increment</button>


    </div>
  )
}

export default LearnUseEffect1