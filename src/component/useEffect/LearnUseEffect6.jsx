import React, { useEffect, useState } from "react";

const LearnUseEffect6 = () => {
  //useEffect with empty dependency

  let [count, setCount] = useState(0);

  // if there is no dependency
  // for every render useEffect code execute
  useEffect(() => {
    console.log("i am useEffect");
  });
  //   useEffect(() => {
  //     console.log("i am useEffect");
  //   },[]);
  //   useEffect(() => {
  //     console.log("i am useEffect");
  //   },[count1,count2]);
  return (
    <div>
      count is {count}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnUseEffect6;
