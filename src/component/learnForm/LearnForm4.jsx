import React, { useState } from "react";

const LearnForm4 = () => {
  let [gender, setGender] = useState("Male");

  return (
    <div>
      <label htmlFor="Male">Male</label>
      <input
        type="radio"
        value="Male"
        id="Male"
        checked={gender === "Male"}
        onChange={(e) => {
          setGender(e.target.value);
        }}
      />

      <label htmlFor="Female">Female</label>
      <input
        type="radio"
        value="Female"
        id="Female"
        checked={gender === "Female"}
        onChange={(e) => {
          setGender(e.target.value);
        }}
      />
      <label htmlFor="Other">Other</label>
      <input
        type="radio"
        value="Other"
        id="Other"
        checked={gender === "Other"}
        onChange={(e) => {
          setGender(e.target.value);
        }}
      />
    </div>
  );
};

export default LearnForm4;
