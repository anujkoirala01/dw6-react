import React, { useRef, useState } from "react";

const HideAndShowPassword = () => {
    let [password,setPassword] = useState("")
    let [show,setShow] = useState(true)
    let useRef1 = useRef()
  return (
    <div>
      <form
     >
        <div>
          <label htmlFor="password">Password</label>
          <input ref={useRef1} type="password" id="password"
          value={password}
          onChange={(e)=>{
            setPassword(e.target.value)
          }}/>
          <button
          type="button"
          onClick={()=>{
            if(show)
            {
                useRef1.current.type="text"
            }
            else
            {
                useRef1.current.type="password"    
            }
            
            setShow(!show)
          }}>{show?"Show":"Hide"}</button>

        </div>
      </form>
    </div>
  );
};

export default HideAndShowPassword;
