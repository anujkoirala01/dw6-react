import React from 'react'

const LearnForm1 = () => {

  return (
    <div>
        <form>
            {/* input element --> general */}
            <input type='text'></input>
            <br />
            <input type='password'></input>
            <br />
            <input type='email'></input>
            <br />
            <input type='number'></input>
            <br />
            <input type='date'></input>
            <br />
            <input type='time'></input>
            <br />
            <input type="file" />
            <br />
            {/* Textarea */}
            <textarea name="" id="" cols="30" rows="10"></textarea>
            <br />

            {/* select */}

            <select>
                <option >Nepal</option>
                <option >India</option>
                <option >Canada</option>
            </select>
            <br />

            {/* special input */}

            <input type="radio" />
            <br />
            <input type="checkbox" />

        </form>

    </div>
  )
}

export default LearnForm1