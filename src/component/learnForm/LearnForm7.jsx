import React, { useState } from "react";
import { useAsyncError } from "react-router-dom";

const LearnForm7 = () => {
  let [firstName, setFirstName] = useState("");
  let [lastName, setLastName] = useState("");
  let [password, setPassword] = useState("");
  let [email, setEmail] = useState("");
  let [phoneNo, setPhoneNo] = useState(0);
  let [description, setDescription] = useState("");
  let [isMarried, setIsMarried] = useState(false);
  let [country, setCountry] = useState("");
  let [gender, setGender] = useState("");
  let [clothSize, setClothSize] = useState("");

  let handleSubmit = (e) => {
    e.preventDefault(); // no refresh
    let info = {
      _firstName: firstName,
      _lastName: lastName,
      _password: password,
      _email: email,
      _phoneNo: phoneNo,
      _description: description,
      _isMarried: isMarried,
      _country: country,
      _gender: gender,
      _clothSize: clothSize,
    };

    console.log(info);
  };
  let countryOption = [
    {
      label: "Select Country",
      value: "",
      disabled: true,
    },
    {
      label: "Nepal",
      value: "Nep",
    },
    {
      label: "India",
      value: "Ind",
    },
    {
      label: "China",
      value: "Chi",
    },
    {
      label: "Japan",
      value: "Jap",
    },
    {
      label: "USA",
      value: "US",
    },
    {
      label: "Germany",
      value: "Ger",
    },
    {
      label: "Korea",
      value: "Kor",
    },
  ];

  let clothSizeOption = [
    {
      label: "Select Size",
      value: "",
      disabled: true,
    },
    {
      label: "Small",
      value: "S",
    },
    {
      label: "Medium",
      value: "M",
    },
    {
      label: "Large",
      value: "L",
    },
    {
      label: "Extra Large",
      value: "XL",
    },
    {
      label: "Double Extra Large",
      value: "XXL",
    },
  ];

  let genderOption = [
    {
      label: "Male",
      value: "Male",
    },
    {
      label: "Female",
      value: "Female",
    },
    {
      label: "Others",
      value: "Others",
    },
  ];

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label htmlFor="firstName">First Name</label>
        <input
          id="firstName"
          type="text"
          placeholder="First Name"
          required
          value={firstName}
          onChange={(e) => {
            // console.log(e.target.value)
            setFirstName(e.target.value);
          }}
        ></input>
        <br></br>
        <br></br>
        <br></br>
        <label htmlFor="lastName">Last Name</label>
        <input
          id="lastName"
          type="text"
          placeholder="Last Name"
          required
          value={lastName}
          onChange={(e) => {
            setLastName(e.target.value);
            // console.log(e.target.value)
          }}
        ></input>

        <br />
        <br />
        <br />

        <label htmlFor="password">Password</label>
        <input
          type="password"
          id="password"
          required
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        />

        <br />
        <br />
        <br />
        <label htmlFor="description">Description</label>
        <textarea
          name="description"
          id="description"
          cols="30"
          rows="10"
          value={description}
          onChange={(e) => {
            setDescription(e.target.value);
          }}
        ></textarea>

        <br />
        <br />
        <br />
        <label htmlFor="country">Country </label>
        <select
          name="select"
          id="country"
          value={country}
          onChange={(e) => {
            setCountry(e.target.value);
          }}
        >
          {countryOption.map((item, i) => {
            return (
              <option key={i} value={item.value} disabled={item.disabled}>
                {item.label}
              </option>
            );
          })}
        </select>

        <br />
        <br />
        <br />

        <label htmlFor="cloth">Cloth Size </label>
        <select
          name="select"
          id="cloth"
          value={clothSize}
          onChange={(e) => {
            setClothSize(e.target.value);
          }}
        >
          {clothSizeOption.map((item, i) => {
            return (
              <option key={i} value={item.value} disabled={item.disabled}>
                {item.label}
              </option>
            );
          })}

          {/* <option value="" disabled={true}>Select Country</option>
          <option value="Nepal">Nepal</option>
          <option value="India">India</option>
          <option value="China">China</option>
          <option value="Japan">Japan</option>
          <option value="USA">America</option>
          <option value="Germany">Germany</option>
          <option value="Korea">Korea</option> */}
        </select>

        <br />
        <br />
        <br />
        {/* 
        <label htmlFor="male">Male</label>
        <input
          type="radio"
          id="male"
          value="male"
          checked={gender === "male"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        />
        <label htmlFor="female">Female</label>
        <input
          type="radio"
          id="female"
          value="female"
          checked={gender === "female"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        />
        <label htmlFor="others">Others</label>
        <input
          type="radio"
          id="others"
          checked={gender === "others"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
          value="others"
        /> */}

        <label>Gender</label>
        {genderOption.map((item, i) => {
          return (
            <div key={i}>
              <label htmlFor={item.value}>{item.value}</label>
              <input
                type="radio"
                id={item.value}
                value={item.value} 
                checked={gender === item.value}
                onChange={(e) => {
                  setGender(e.target.value);
                }}
              />
            </div>
          );
        })}

        <br />
        <br />
        <br />

        <label htmlFor="isMarried">Is Married?</label>
        <input
          type="checkbox"
          id="isMarried"
          checked={isMarried}
          onChange={(e) => {
            setIsMarried(e.target.checked);
          }}
        />

        <br />
        <br />
        <br />

        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default LearnForm7;
