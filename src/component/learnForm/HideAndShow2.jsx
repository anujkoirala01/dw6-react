import React, { useRef, useState } from "react";

const HideAndShow2 = () => {
    let [password,setPassword] = useState("")
    let [type,setType] = useState("password")
    let [show, setShow] = useState(true)
    let useRef1 = useRef()
  return (
    <div>
      <form
     >
        <div>
          <label htmlFor="password">Password</label>
          <input ref={useRef1} type={type} id="password"
          value={password}
          onChange={(e)=>{
            setPassword(e.target.value)
          }}/>
          <button
          type="button"
          onClick={()=>{
            if(type === "password")
            {
                setType("text")
            }
            else if(type === "text")
            {
                setType("password")
            }
            
            setShow(!show)
          }}>{show?"Show":"Hide"}</button>

        </div>
      </form>
    </div>
  );
};

export default HideAndShow2;
