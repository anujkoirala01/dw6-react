import React, { useState } from "react";

const LearnForm2 = () => {
  let [name, setName] = useState("");
  let [age,setAge] = useState()
  let [address,setAddress] = useState("")
  let [password,setPassword] = useState("")
  let [des,setDes] = useState("")

  return (
    <div>
      <form>

        <div>
        <label htmlFor="name">Name:</label>
        <input
          id="name"
          type="text"
          placeholder="Enter your name"
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
        ></input>
        </div>
        <br />

        <div>
            <label htmlFor="age">Age:</label>
            <input 
            type="number" 
            id="age"
            value={age}
            onChange={(e)=>{
                setAge(e.target.value)

            }}  />
        </div>
        <br />
        <div>
            <label htmlFor="address">Address:</label>
            <input 
            type="text" 
            id="address"
            value={address}
            onChange={(e)=>{
                setAddress(e.target.value)

            }}  />
        </div>
        <br />
        <div>
            <label htmlFor="password">Password:</label>
            <input 
            type="password" 
            id="password"
            value={password}
            onChange={(e)=>{
                setPassword(e.target.value)

            }}  />
        </div>
        <br />

        <div>
            <label htmlFor="des">Description:</label>
            <br />
            <textarea
            type="" 
            id="des"
            cols={30}
            rows={10}
            value={des}
            onChange={(e)=>{
                setDes(e.target.value)

            }}  />
            <br />
        </div>

        <button
        onClick={()=>{
            setName("")
        }}
        type="button">Clear</button>

        <button
        onClick={()=>{
            setName("Hari")
        }}
        type="button">Hari</button>

        

      </form>
    </div>
  );
};

export default LearnForm2;
