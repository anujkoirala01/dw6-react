import React, { useState } from 'react'


// checkbox
// generally used for two options
const LearnForm5 = () => {
    let [isMarried,setIsMarried] = useState(false)


  return (
    <div>

        <label htmlFor="isMarried">Are you married?</label>
        <input 
        type="checkbox"
        id='isMarried'
        checked={isMarried}
        onChange={(e)=>{
            setIsMarried(e.target.checked)
        }} />


    </div>
  )
}

export default LearnForm5