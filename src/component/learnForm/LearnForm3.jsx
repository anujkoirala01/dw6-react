import React, { useState } from 'react'

const LearnForm3 = () => {
    let [country,setCountry] = useState("")
    
  return (
    <div>
        <form>
            <select
            value={country}
            onChange={(e)=>{
                setCountry(e.target.value)  // option value
            }}>
                <option value="Nepal">Nepal</option>
                <option value="India">India</option>
                <option value="China">China</option>
                <option value="Pakistan">Pakistan</option>
            </select>
        </form>

    </div>
  )
}

export default LearnForm3