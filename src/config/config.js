export const baseUrl = process.env.REACT_APP_BASEURL;

export const companyOptions = [
  {
    label: "Select Company",
    value: "",
    disabled: true,
  },
  {
    label: "Apple",
    value: "apple",
  },
  {
    label: "Samsung",
    value: "samsung",
  },
  {
    label: "Dell",
    value: "dell",
  },
  {
    label: "MI",
    value: "mi",
  },
];
