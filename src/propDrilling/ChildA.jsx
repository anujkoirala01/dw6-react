import React from 'react'
import ChildB from './ChildB'

const ChildA = (props) => {
    console.log(props.name)
  return (
    <div>ChildA
        <ChildB name={props.name}></ChildB>
    </div>
  )
}

export default ChildA