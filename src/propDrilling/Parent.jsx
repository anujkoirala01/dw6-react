import React, { useState } from "react";
import ChildA from "./ChildA";

const Parent = () => {
  let [name, setName] = useState("anuj");
  return (
    <div>
      Parent
      <ChildA name={name}></ChildA>
    </div>
  );
};

export default Parent;
