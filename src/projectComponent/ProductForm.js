import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "../component/learnFormik/FormikInput";
import FormikCheckbox from "../component/learnFormik/FormikCheckbox";
import FormikSelect from "../component/learnFormik/FormikSelect";
import htmlDateFormat from "../ProjectUtils/inputDateFormat";
import { companyOptions } from "../config/config";

const ProductForm = ({
  buttonName = "Create Product",
  onSubmit = () => {},
  product = {},
  isLoading=false
}) => {
  let initialValues = {
    name: product.name || "",
    quantity: product.quantity || 1,
    price: product.price || 100,
    featured: product.featured || false,
    productImage: product.productImage || "",
    manufactureDate: htmlDateFormat(product.manufactureDate || new Date()),
    company: product.company || "apple",
  };

  let validationSchema = yup.object({
    name: yup.string().required("Name is required"),
    quantity: yup.number().required("Quantity is required"),
    price: yup.number().required("Price is required"),
    featured: yup.boolean(),
    productImage: yup.string().required("Product image is required"),
    manufactureDate: yup.string().required("Manufacture Date is required"),
    company: yup.string().required("Company is required"),
  });


  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
        enableReinitialize={true}
      >
        {(formik) => {
          // name
          // label
          // type : formikInput only
          // onchange
          // required
          // option(radio,select) --> array of object
          return (
            <Form>
              <FormikInput
                name="name"
                label="Name"
                type="text"
                required={true}
              ></FormikInput>

              <FormikInput
                name="quantity"
                label="Quantity"
                type="number"
                required={true}
              ></FormikInput>

              <FormikInput
                name="price"
                label="Price"
                type="number"
                required={true}
              ></FormikInput>

              <FormikCheckbox name="featured" label="Featured"></FormikCheckbox>

              <FormikInput
                name="productImage"
                label="Product Image"
                type="text"
                required={true}
              ></FormikInput>

              <FormikInput
                name="manufactureDate"
                label="Manufacture Date"
                type="date"
                required={true}
              ></FormikInput>

              <FormikSelect
                name="company"
                label="Company"
                required={true}
                options={companyOptions}
              ></FormikSelect>

              <button type="submit">
                {
                  isLoading?(<div>Creating</div>):(<div>{buttonName}</div>)
                }
                </button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default ProductForm;
