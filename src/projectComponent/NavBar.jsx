import React from 'react'
import { NavLink } from 'react-router-dom'

const NavBar = () => {
  return (
    <div>
        <nav style={{backgroundColor:"lightblue"}}>
        <NavLink to="/products" style={{ marginLeft: "20px" }}>
        Products
      </NavLink>
      <NavLink to="/products/create" style={{ marginLeft: "20px" }}>
        Create Product
      </NavLink>
      {/* <NavLink to="/products/:id" style={{ marginLeft: "20px" }}>
        Detail Page
      </NavLink> */}
        </nav>
    </div>
  )
}

export default NavBar