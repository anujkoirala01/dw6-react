import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { hitApi } from "../services/hitApi";

const ViewProduct = () => {
  let [product, setProduct] = useState({});
  
  let params = useParams();
  let id = params.id;
  
  let getProduct = async () => {
    try {
      let output = await hitApi({
        method: "get",
        url: `/products/${id}`,
      });

      setProduct(output.data.data);

      console.log(output.data.data);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getProduct();
  }, []);
  return (
    <div>
      <img
        src={product.productImage}
        alt="Router Image"
        style={{height:"250px", width:"250px" }}
      />
      <p>Product name: {product.name}</p>
      <p>Product company: {product.company}</p>
      <p>Is featured?: {product.featured ? "Yes" : "No"}</p>
      <p>
        Manufacture Date:{" "}
        {new Date(product.manufactureDate).toLocaleDateString()}
      </p>
      <p>Price: {product.price}</p>
      <p>Quantity: {product.quantity}</p>
    </div>
  );
};

export default ViewProduct;
