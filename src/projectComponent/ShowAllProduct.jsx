import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { hitApi } from "../services/hitApi";


const ShowAllProduct = () => {

    let [products, setProducts] = useState([])

    let navigate = useNavigate()

  let getProduct = async () => {
    try {
        let output = await hitApi({
            method: "get",
            url: "/products",
          });
      
        setProducts(output.data.data.results)

        
    } catch (error) {
        console.log(error.message)
        
    }


  };


// We cannot use async keyword in useEffect function
  useEffect(() => {
    getProduct()
  }, []);

  let handleView = (item)=>{
    return()=>{
        navigate(`/products/${item._id}`)
    }
  }

  // Function that return function
  // It is used when something needs to be passed from the called block

  let handleEdit = (item)=>{
    return()=>{
        navigate(`/products/update/${item._id}`) 
    }
  }


  return (<div>
    {
        products.map((item,i)=>{
            return(<div key={i}
            style={{border:"solid red 2px", marginBottom:"5px"}}>
                <img src={item.productImage} alt="product img"
                style={{width:"80px", height:"80px"}} />
                <br />
                name:{item.name}
                <br />
                price:{item.price}
                <br />
                Quantity:{item.quantity}
                <br />
                Company:{item.company}
            <br />

            <button
            type="button"
            onClick={async()=>{
                try {
                    await hitApi({
                       method:"delete",
                       url:`/products/${item._id}`
                   })
           
                   getProduct()
                   
               } catch (error) {
                   console.log(error.message)  
               }

            }}>Delete Product</button>

            <button
            type="button"
            onClick={handleView(item)}>View Product</button>

            <button
            type="button"
            onClick={handleEdit(item)}>Edit Product</button>
            </div>)
        })
    }
  </div>)
};

export default ShowAllProduct;
