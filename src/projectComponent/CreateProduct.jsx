import React from "react";
import ProductForm from "./ProductForm";
import { useNavigate } from "react-router-dom";
import { hitApi } from "../services/hitApi";
import axios from "axios";

const CreateProduct = () => {
  let navigate = useNavigate();

  let onSubmit = async (values, other) => {
    try {
      let output = await hitApi({
        method: "post",
        url: "/products",
        data: values,
      });

      navigate("/products");
    } catch (error) {
      console.log(error.message);
    }
    // console.log(value);
    // hit api
  };

  return (
    <div>
      <ProductForm
        buttonName="Create Product"
        onSubmit={onSubmit}
      ></ProductForm>
    </div>
  );
};

export default CreateProduct;
