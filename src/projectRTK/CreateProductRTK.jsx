import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import ProductForm from "../projectComponent/ProductForm";
import { useCreateProductMutation } from "../services/api/productService";

const CreateProductRTK = () => {
  let navigate = useNavigate();

  let [createProduct, 
    {isError:isErrorCreateData,
        isLoading:isLoadingCreateData,
        isSuccess:isSuccessCreateData,
        error:errorCreateData,
        data:dataCreateData}] = useCreateProductMutation()

    

    useEffect(()=>{
        if(isSuccessCreateData)
        {
            console.log("Successfully Created")
        }
    },[isSuccessCreateData])


    useEffect(()=>{
        if(isErrorCreateData)
        {
            console.log(errorCreateData.error)
        }
    },[isErrorCreateData,errorCreateData])

  let onSubmit = async (values, other) => {
    let body = values
    createProduct(body)
    navigate("/products")

  };

  return (
    <div>
      <ProductForm
        buttonName="Create Product"
        onSubmit={onSubmit}
        isLoading={isLoadingCreateData}
      ></ProductForm>
    </div>
  );
};

export default CreateProductRTK;
