import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import ProductForm from "./ProductForm";
import { hitApi } from "../services/hitApi";
import axios from "axios";

const UpdateProduct = () => {
  let [product, setProduct] = useState({});

  let navigate = useNavigate();

  let params = useParams();
  let id = params.id;

  let getProduct = async () => {
    try {
      let output = await hitApi({
        method: "get",
        url: `/products/${id}`,
      });

      setProduct(output.data.data);

      console.log(output.data.data);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getProduct();
  }, []);

  let onSubmit = async (values, other) => {
    try {
      let output = await hitApi({
        method: "patch",
        url: `/products/${id}`,
        data: values,
      });
      navigate(`/products/${id}`);
    } catch (error) {
      console.log(error.message);
    }
    // console.log(value);
    // hit api
  };

  return (
    <div>
      <ProductForm
        buttonName="Update Product"
        onSubmit={onSubmit}
        product={product}
      ></ProductForm>
    </div>
  );
};

export default UpdateProduct;
