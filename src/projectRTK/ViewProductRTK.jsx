import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useReadProductByIdQuery } from "../services/api/productService";

const ViewProductRTK = () => {

  let params = useParams();
  let id = params.id;

  let {isError:isErrorReadDetails, 
    isLoading:isLoadingReadDetails,
    isSuccess:isSuccessReadDetails,
    data:dataReadDetails,
    error:errorReadDetails}=
     useReadProductByIdQuery(id)
     
     
  let product = dataReadDetails?.data || {}

  useEffect(()=>{
    if(isErrorReadDetails)
    {
      console.log(errorReadDetails.error)
    }
  },[isErrorReadDetails,errorReadDetails])


  return (
    <div>
      {isLoadingReadDetails?<div><h1>Loading...</h1></div>:    <div>
      <img
        src={product.productImage}
        alt="Router"
        style={{ height: "250px", width: "250px" }}
      />
      <p>Product name: {product.name}</p>
      <p>Product company: {product.company}</p>
      <p>Is featured?: {product.featured ? "Yes" : "No"}</p>
      <p>
        Manufacture Date:{" "}
        {new Date(product.manufactureDate).toLocaleDateString()}
      </p>
      <p>Price: {product.price}</p>
      <p>Quantity: {product.quantity}</p>
    </div>}

    </div>
  );
};

export default ViewProductRTK;
