import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDeleteProductMutation, useReadProductQuery } from "../services/api/productService";


const ShowAllProductUsingRTK = () => {
    let {isError:isErrorReadProduct, isSuccess:isSuccessReadProduct, isLoading:isLoadingReadProduct, data:dataReadProduct, error:errorReadProduct} = useReadProductQuery()
    let [deleteProduct, {isError:isErrorDeleteProduct,isSuccess:isSuccessDeleteProduct, isLoading:isLoadingDeleteProduct, data:dataDeleteProduct, error:errorDeleteProduct}] = useDeleteProductMutation()

    // console.log(data?.data?.results)

    useEffect(()=>{
        if(isErrorReadProduct)
        {
            console.log(errorReadProduct?.error)
        }

    },[isErrorReadProduct,errorReadProduct])

    useEffect(()=>{
        if(isErrorDeleteProduct)
        {
            console.log(errorReadProduct?.error)
        }

    },[isErrorDeleteProduct,errorDeleteProduct])

    useEffect(()=>{
        if(isSuccessDeleteProduct)
        {
            console.log("Product is deleted successfully.")
        }
    })

    let products = dataReadProduct?.data?.results || []

    let navigate = useNavigate()


  let handleView = (item)=>{
    return()=>{
        navigate(`/products/${item._id}`)
    }
  }

  // Function that return function
  // It is used when something needs to be passed from the called block

  let handleEdit = (item)=>{
    return()=>{
        navigate(`/products/update/${item._id}`) 
    }
  }


  return (
    <div>
        {
            isLoadingReadProduct?<div><h1>... Loading</h1></div>: <div>
            {
                products?.map((item,i)=>{
                    return(<div key={i}
                    style={{border:"solid red 2px", marginBottom:"5px"}}>
                        <img src={item.productImage} alt="product img"
                        style={{width:"80px", height:"80px"}} />
                        <br />
                        name:{item.name}
                        <br />
                        price:{item.price}
                        <br />
                        Quantity:{item.quantity}
                        <br />
                        Company:{item.company}
                    <br />
        
                    <button
                    type="button"
                    onClick={async(e)=>{
                        deleteProduct(item._id)
                    }}>{isLoadingDeleteProduct?"Deleting":"Delete Product"}</button>
        
                    <button
                    type="button"
                    onClick={handleView(item)}>View Product</button>
        
                    <button
                    type="button"
                    onClick={handleEdit(item)}>Edit Product</button>
                    </div>)
                })
            }
          </div>
        }
 
  </div>)

};

export default ShowAllProductUsingRTK;
