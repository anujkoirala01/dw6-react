import { baseUrl } from "../../config/config";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

// Generally, we provide tags in query and invalidate
// them in mutation

export const productApi = createApi({
  // Reducer path must be unique
  reducerPath: "productApi",
  baseQuery: fetchBaseQuery({
    baseUrl: baseUrl,
  }),
  // 1. Creating Tag
  tagTypes: ["readProduct","readProductById"],

  // query and mutation
  // query --> Method: get (Page render)
  // mutation --> Method: remaining (button click )
  endpoints: (builder) => ({
    readProduct: builder.query({
      query: () => {
        return {
          url: "/products",
          method: "GET",
        };
      },
      // 2. Providing tag
      providesTags:["readProduct"]
    }),
    deleteProduct: builder.mutation({
      query: (id) => {
        return {
          url: `/products/${id}`,
          method: "DELETE",
        };
      },
      // 3.  Invalidating tag
      invalidatesTags:["readProduct"]
    }),
    readProductById: builder.query({
      query: (id) => {
        return {
          url: `/products/${id}`,
          method: "GET",
        };
      },
      providesTags:["readProductById"]
    }),
    createProduct: builder.mutation({
      query: (body) => {
        return {
          url: "/products",
          method: "POST",
          body:body
        };
      },
      invalidatesTags:["readProduct"]
    }),
    // invalidate detail page while updating
  }),
});

export const { useReadProductQuery, useDeleteProductMutation, useReadProductByIdQuery, useCreateProductMutation } = productApi;
