import { configureStore } from "@reduxjs/toolkit";
import counterSlice from "../features/counterSlice";
import addressSlice from "../features/addressSlice";
import productSlice from "../features/productSlice";
import blogSlice from "../features/blogSlice";
import { productApi } from "../services/api/productService";


export const store = configureStore({
    reducer:{
        info: counterSlice,
        address: addressSlice,
        product: productSlice,
        blog: blogSlice,

        [productApi.reducerPath] : productApi.reducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware()
    .concat([productApi.middleware])
})